import java.util.*;

public class MyHashTable<T> {
   
 LinkedList<T>[] array;
   
   public MyHashTable(int size) {
      array = (LinkedList<T>[]) new LinkedList[size];
      for(int i=0; i<size; i++) {
         array[i] = new LinkedList<T>();
      }
   }
   
   private int hash(T x) {
      
      return Math.abs(x.hashCode() % array.length);
      
   }
   
   public void insert(T data) {
      
      int location = hash(data);
      array[location].push(data);
      
   }
   
   public void delete(T data) {
      
      int location = hash(data);
      LinkedList<T> temp = array[location];
      
      for(int i=0; i<temp.size(); i++) {
         if(temp.get(i) == data)
            array[location].remove(i);
      }

   }
   
   public boolean find(T data) {
      
      int location = hash(data);
      LinkedList<T> temp = array[location];
      
      for(int i=0; i<temp.size(); i++) {
         if(temp.get(i) == data)
            return true;
      }
      
      return false;
      
   }
   
   public boolean isEmpty() {
      
      for(int i=0; i<array.length; i++) {
         if(!array[i].isEmpty())
            return false;
      }
      
      return true;
   
   }
   
   public void print() {
      
      for(int i=0; i<array.length; i++) {
         
         System.out.print(i + ": ");
         
         for(int j=0; j<array[i].size(); j++) {
            
            System.out.print(array[i].get(j) + " ");
            
         }
         
         System.out.println();
         
      }
      
   }
   
   public void makeEmpty() {
      
      for(int i=0; i<array.length; i++)
         array[i].clear();
      
   }
   
   public int size() {
      
      int sum = 0;
      
      for(int i=0; i<array.length; i++)
         sum += array[i].size();
      
      return sum;
      
   }
   
   private class Iter implements Iterator<T> {
      
      private int i;
      private int j;
      
      public Iter() {
         i = 0;
         while(i<array.length && array[i].isEmpty())
            i++;
         j = 0;
      }
      
      public boolean hasNext(){
         
         if(i < array.length)
            return true;
         else
            return false;
         
      }
      
      public T next() {
         
         T temp;
         
         if(!hasNext()) {            
            throw new NoSuchElementException();            
         }
         else {
            temp = array[i].get(j);
            
            if(j+1 < array[i].size())
               j++;
            else {
               i++;
               j = 0;
               while(i < array.length && array[i].size() == 0) {
                  i++;
               }
            }            
         }
            
         return temp;
         
      }
      
      public void remove(){
         
         throw new UnsupportedOperationException();
         
      }
      
   }
   
   public Iterator<T> iterator() {
      
      return new Iter();
      
   }
   
   
   
}

import java.util.*;


public class HashTest {
   
   public static void main(String[] args) {
      
      Scanner input = new Scanner(System.in);
      System.out.println("Enter the size of hash table: ");
      int size = input.nextInt();
      input.nextLine();
      char choice = 'z';
      Integer temp;
    
      MyHashTable<Integer> intHash = new MyHashTable<Integer>(size);
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- add/insert (enter the letter a)");
      System.out.println("- find (enter the letter f)");
      System.out.println("- delete (enter the letter d)");
      System.out.println("- is empty (enter the letter e)");
      System.out.println("- print (enter the letter p)");
      System.out.println("- make empty (enter the letter k)");
      System.out.println("- size (enter the letter s)");
      System.out.println("- output (enter the letter o)");
      System.out.println("- quit (enter the letter q)");

      while(choice != 'q') {
         
         System.out.println("Enter choice:");
         choice = input.nextLine().charAt(0);
         
         switch(choice) {
         
         case 'a':
                  System.out.println("Enter a number to be added: ");
                  temp = input.nextInt();
                  input.nextLine();
                  intHash.insert(temp);
                  System.out.println(temp + " has been inserted");
                  break;
         case 'f':
                  System.out.println("Enter a number to find: ");
                  temp = input.nextInt();
                  input.nextLine();
                  if(intHash.find(temp))
                     System.out.println(temp + " has been found");
                  else
                     System.out.println(temp + " is NOT found");
                  break;
         case 'd':
                  System.out.println("Enter a number to delete: ");
                  temp = input.nextInt();
                  input.nextLine();
                  intHash.delete(temp);
                  System.out.println(temp + " has been deleted");
                  break;
         case 'e':
                  if(intHash.isEmpty())
                     System.out.println("Empty");
                  else
                     System.out.println("NOT empty");
                  break;
         case 'p':
                  intHash.print();
                  break;
         case 'k':
                  intHash.makeEmpty();
                  System.out.println("Hash table is cleared!");
                  break;
         case 's':
                  System.out.println("Hash table size is " + intHash.size());
                  break;
         case 'o':
                  Iterator<Integer> itr = intHash.iterator();
                  while(itr.hasNext())
                     System.out.print(itr.next() + " ");
                  System.out.println();
                  break;
         case 'q':
                  System.out.print("Quitting");
                  break;
         default:
                  System.out.print("Invalid choice");
                  break;
                  
         }
      }
   }
}
